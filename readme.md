client-server
=============

Access point for end users

oauth2-server
=============

OAuth2 Authorization server

resource-server
===============

Server hosting resources. This server verifies the OAuth2 access token at the oauth2-server