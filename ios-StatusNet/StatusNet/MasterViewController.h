//
//  MasterViewController.h
//  StatusNet
//
//  Created by Robert Loghem on 11/16/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UITableView *tweetTableView;
@property (retain, nonatomic) IBOutlet UITextField *textField;
- (IBAction)sendUpdate:(id)sender;
- (IBAction)login:(id)sender;

-(void)reloadTimeLineData;

@end
