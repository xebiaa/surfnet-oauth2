//
//  MasterViewController.m
//  StatusNet
//
//  Created by Robert Loghem on 11/16/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "MasterViewController.h"
#import "MGTwitterEngine.h"

@interface MasterViewController () {
    NSMutableArray *updates;
    MGTwitterEngine *twitterEngine;
}

-(void)loadLatestTimeLineData;

@end

@implementation MasterViewController

@synthesize tweetTableView = _tweetTableView;
@synthesize textField = _textField;

- (IBAction)sendUpdate:(id)sender {
    [twitterEngine sendUpdate:self.textField.text];
    self.textField.text = @"";
    [self.textField resignFirstResponder];
    [self loadLatestTimeLineData];
}

- (IBAction)login:(id)sender {
    NSString *oauth2URL = [[NSUserDefaults standardUserDefaults] stringForKey:@"oauth2_preference"];
    if (!oauth2URL) {
        oauth2URL = @"http://statusnet.surfnetlabs.nl:8000/status.html";
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:oauth2URL]];
}

-(void)reloadTimeLineData {
    [updates removeAllObjects];
    [self loadLatestTimeLineData];
}

-(void)loadLatestTimeLineData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [twitterEngine setUsername:[defaults stringForKey:@"username_preference"] password:[defaults stringForKey:@"password_preference"]];
    [twitterEngine setAPIDomain:[defaults stringForKey:@"domain_preference"]];
    [twitterEngine setUsesSecureConnection:YES]; //TODO, Make it configurable... 

    NSString *publicTimeLine = [twitterEngine getPublicTimeline];
    NSLog(@"Public timeline; %@", publicTimeLine);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Status.net", @"Status.net");
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Reload" style:UIBarButtonItemStylePlain target:self action:@selector(reloadTimeLineData)] autorelease];
    }
    return self;
}
							
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    twitterEngine = [[MGTwitterEngine alloc] initWithDelegate:self];
    updates = [[NSMutableArray alloc] init];
    
    [self loadLatestTimeLineData];
}

- (void)viewDidUnload
{
    [self setTweetTableView:nil];
    [self setTextField:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    [twitterEngine release];
    [updates release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark twitter delegate methods

- (void)requestSucceeded:(NSString *)connectionIdentifier {
    NSLog(@"request succeeded; %@", connectionIdentifier);
}

- (void)requestFailed:(NSString *)connectionIdentifier withError:(NSError *)error {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"An Error occurred" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    [alert release];
}

- (void)statusesReceived:(NSArray *)statuses forRequest:(NSString *)connectionIdentifier {
    NSLog(@"Received status updates... number is %d", [statuses count]);

    NSRange range = {0, [statuses count]};
    [updates insertObjects:statuses atIndexes:[NSIndexSet indexSetWithIndexesInRange:range]];
    [self.tweetTableView reloadData];
}

#pragma mark UITableViewDelegate methods

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *username = [[NSUserDefaults standardUserDefaults] stringForKey:@"username_preference"];
    if (!username || [@"" isEqualToString:username]) {
        username = @"Public twitter";
    }
    return [NSString stringWithFormat:@"%@'s timeline", username];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [updates count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CELL_ID = @"tweetCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_ID];
    if (!cell) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CELL_ID] autorelease];
        cell.textLabel.numberOfLines = 2;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSDictionary *tweet = [updates objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [tweet objectForKey:@"text"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"by %@", [[tweet objectForKey:@"user"] objectForKey:@"screen_name"]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 88;
}

- (void)dealloc {
    [_tweetTableView release];
    [_textField release];
    [updates release];
    [twitterEngine release];
    [super dealloc];
}
@end
