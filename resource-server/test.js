var sys = require("sys"),  
    http = require("http");  

var options = {
  host: 'localhost',
  port: 9090,
  path: '/oauth/validate',
  method: 'POST'
};

var resourceServerResponseCounter=0;
var resourceServerResponse=[];

/*
 * make request to authorization server and send response
 * back to the original response.
 */
function createASRequest(id, accessToken){
	var req = http.request(options, function(res){
		console.log(id);
		console.log('STATUS: ' + res.statusCode);
		console.log('HEADERS: ' + JSON.stringify(res.headers));
		res.setEncoding('utf8');
		res.on('data', function (chunk) {
			console.log('BODY: ' + chunk);
			// resourceServerResponse[id].write("console.log('" + chunk + "')\n");  
			resourceServerResponse[id].write("callback(" + chunk + ")\n");  
		   resourceServerResponse[id].end();
		});
	});

	req.on('error', function(e) {
		console.log('problem with request: ' + e.message);
	});

	var json=  { access_token: accessToken
	          , localOnly:true
	          , uri:"http://localhost:8000/status.html"
	          // , scope: ["test2"] 
	};

	// write data to request body
	req.write(JSON.stringify(json));
	req.end();
}

var extractToken = function(hash) {
  if (hash) {
  	var match = hash.match(/access_token=(\w+)/);
  	return !!match && match[1];
  } else {
	return "";
  }
};

/**
 * Start server at port 8080 
 */
http.createServer(function(request, response) {  
	console.log("query", require('url').parse(request.url).query);
	var accessToken=extractToken(require('url').parse(request.url).query);
	console.log("access token: " + accessToken);
	
	resourceServerResponse[resourceServerResponseCounter]=response;
	response.statusCode = 200;
    response.setHeader("Content-Type", "application/json");  
	createASRequest(resourceServerResponseCounter, accessToken);
	resourceServerResponseCounter++;
}).listen(8080);  

sys.puts("Server running at http://localhost:8080/");